TEXFILES := $(shell find src/ -print)
TIKZS = $(patsubst src/figures/dot/%.dot, src/figures/auto-tikz/%.tex, $(wildcard src/figures/dot/*.dot))
GRANTFILES := $(shell find ethereum-academic-grant-24/ -print)

quick: $(TEXFILES)
	export TEXINPUTS=.:./src//:; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode -jobname=incoercible-sale incoercible-sale.tex; \
	touch quick; \
	rm -f *.aux *.log *.out *.toc *.lof *.lot *.bbl *.blg *.xml *-blx.bib

bib: $(TIKZS) $(TEXFILES)
	export TEXINPUTS=.:./src//:; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode incoercible-sale.tex; \
	bibtex incoercible-sale.aux; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode incoercible-sale.tex; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode incoercible-sale.tex; \
	touch quick bib; \
	rm -f *.aux *.log *.out *.toc *.lof *.lot *.bbl *.blg *.xml *-blx.bib

incoercible-sale.pdf: clean bib

figures: $(TIKZS)

src/figures/manual-tikz/*:

src/figures/auto-tikz/%.tex: src/figures/dot/%.dot
	mkdir -p src/figures/auto-tikz/
	dot2tex --texmode math --format tikz --figonly --autosize --usepdflatex --nominsize --prog dot $< > $@

ethereum-academic-grant-24.pdf: grant

grant: $(GRANTFILES)
	export TEXINPUTS=.:./ethereum-academic-grant-24//:; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode -jobname=ethereum-academic-grant-24 grant.tex; \
	bibtex ethereum-academic-grant-24.aux; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode -jobname=ethereum-academic-grant-24 grant.tex; \
	pdflatex --shell-escape -halt-on-error -interaction=nonstopmode -jobname=ethereum-academic-grant-24 grant.tex; \
	rm -f *.aux *.log *.out *.toc *.lof *.lot *.bbl *.blg *.xml *-blx.bib

clean:
	rm -rf *.aux *.log *.out *.toc *.lof *.lot *.bbl *.blg *.xml *-blx.bib *.pdf quick bib src/figures/auto-tikz/
